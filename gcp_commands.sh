
# For more info read https://github.com/oscar6echo/jupyter-on-google-cloud

# on local machine
gcloud auth login

gcloud config list

gcloud config set project testing-pythran
gcloud config set compute/zone us-east4-c 

#### after creating VM from console
gcloud compute instances list

gcloud compute config-ssh

ssh jupyter-notebook-server.us-east4-c.testing-pythran
gcloud compute --project "testing-pythran" ssh --zone "us-east4-c" "jupyter-notebook-server"


# on VM
sudo apt-get update
sudo apt-get -y install bzip2 wget git gcc g++

miniconda="Miniconda3-4.5.11-Linux-x86_64.sh"
wget -P Downloads/ https://repo.continuum.io/archive/${miniconda}
bash ~/Downloads/${miniconda}


conda create -n pythran python=3
source activate pythran
conda install -c conda-forge pythran
conda install notebook matplotlib pandas

python -m ipykernel install --user --name pythran --display-name Pythran

git clone https://gitlab.com/oscar6echo/tsp-pythran.git
cd tsp-pythran

jupyter notebook --no-browser --port=9888


# on local machine
ssh jupyter-notebook-server.us-east4-c.testing-pythran -NL 9888:localhost:9888
gcloud compute ssh --project "remotejupyter" --zone "europe-west3-b" "myserver" -NL 9888:localhost:9888


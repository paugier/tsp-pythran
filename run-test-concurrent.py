from tsp_concurrent import TSP

nb_city = 20
nb_run = int(4)
nb_step = int(1e5)

beta_mult = 1.02
accept_nb_step = 100
p1 = 0.2
p2 = 0.8

tsp = TSP(nb_city, nb_run, nb_step, beta_mult, accept_nb_step, p1, p2)

tsp.generate_cities(seed=54321)


tsp.search("search", dated=True)

print(tsp.res)
